import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InvalidLoginTest extends TestBase{

    @Test(priority = 4, dataProvider = "inValid Login Data")
    public void InValidLoginTC(String Email , String Password)
    {
        new HomePage(driver)
                .clickingOnSignUp_logInButton()
                .enterLoginCredentials(Email,Password)
                .clickOnLoginButton();
        String actual = driver.findElement(By.xpath("//p[@style]")).getText();
        String expected = "Your email or password is incorrect!";
        Assert.assertEquals(actual,expected);
    }

}
