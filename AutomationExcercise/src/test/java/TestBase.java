import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.time.Duration;

public class TestBase {
    protected WebDriver driver;

    @BeforeClass
    public void setUp()
    {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://automationexercise.com/");
    }


    @DataProvider(name = "inValid Login Data")
    public static Object [][] inValidLoginData()
    {
        return new Object[][]{
                {"A.B@g.com","11111"},
                {"Z@t.com","1223344"},
                {"S@B.com","555666777"}
        };

    }


    @AfterClass

    public void quit()
    {
        driver.quit();
    }

}
